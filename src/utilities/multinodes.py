# -*- coding: utf-8 -*-
import tensorflow as tf

FLAGS = tf.app.flags.FLAGS


def get_device_and_target():
    # If FLAGS.job_name is not set, we're running single-machine TensorFlow.
    if FLAGS.job_name is None:
        print("Running single-machine training")
        return (None, "")
    if FLAGS.job_name != "worker":
        print( "Whoops, the specified job_name is not `worker`" );
        return (None,"")    
    

    # Otherwise we're running distributed TensorFlow.
    print("Running distributed training")
    if FLAGS.task_index is None or FLAGS.task_index == "":
        raise ValueError("Must specify an explicit `task_index`")
    if FLAGS.ps_hosts is None or FLAGS.ps_hosts == "":
        raise ValueError("Must specify an explicit `ps_hosts`")
    if FLAGS.worker_hosts is None or FLAGS.worker_hosts == "":
        raise ValueError("Must specify an explicit `worker_hosts`")

    worker_nodes = FLAGS.worker_hosts.split(',');
    server_nodes = FLAGS.ps_hosts.split(',');
    
    #Create a cluster of parameter servers and worker hosts
    cluster = tf.train.ClusterSpec( { "worker": worker_nodes, "ps": server_nodes } );
    server = tf.train.Server( cluster, job_name=FLAGS.job_name, task_index=FLAGS.task_index );

    worker_device = "/job:worker/task:%d" % FLAGS.task_index;
    # The device setter will automatically place Variables ops on separate
    # parameter servers (ps). The non-Variable ops will be placed on the workers.
    return tf.train.replica_device_setter( worker_device=worker_device, cluster=cluster), server.target;
    