# -*- coding: utf-8 -*-
"""
Created on Wed Aug 22 14:49:02 2018

@author: mohacsii
"""
import tensorflow as tf
import sys,os
sys.path.append("..")

from fileio import fileio_cls
from fileio import fileio_seg
from fileio import imagestream_cls
from fileio import imagestream_seg 
from graph import build_graph
from diag import diag

FLAGS = tf.app.flags.FLAGS

def build_file_lists( datapath, num_class, seed=123456 ):
    """Read training file names"""
    if FLAGS.model_type in ["cls","classification"]: 
        filelist_raw,num_class = fileio_cls.get_train_filelist( datapath, FLAGS.data_style ).load_filelist();
    elif FLAGS.model_type in ["seg","segmentation"]: 
        filelist_raw = fileio_seg.get_train_filelist_salt( datapath );   
    filelist_train = filelist_raw.sample( frac=FLAGS.test_split, random_state=seed );
    filelist_valid = filelist_raw.drop( filelist_train.index ); 
    return filelist_train,filelist_valid,num_class

def build_graph_ops( device, is_chief, num_class, d_in=5, oldvar=None ):
    """Assigns ops to the local worker by default."""
    with tf.device(device):
        if FLAGS.do_load_checkpoint==1:
            loader = build_graph.model_template.load_old_graph( FLAGS.loadfile_path, FLAGS.loadfile_name )  
        else:   
            loader=None;
            """Set up computation graph"""
            cgraph = build_graph.get_model_from_library(FLAGS.model_name, num_class, FLAGS.regularizer_l2, FLAGS.regularizer_sl, mtype=FLAGS.model_type );
            cgraph.set_model_width( FLAGS.model_width );
            cgraph.create_new_model( d_in=d_in );
            cgraph.load_graph_checkpoints();

            """Set up diagnostics"""
            diag.print_num_trainpar( FLAGS.summary_path );
            if FLAGS.model_type in ["cls","classification"]: 
                train_writer,valid_writer,merged_summary = diag.prepare_summary_writer_cls( cgraph, FLAGS.summary_path );
            elif FLAGS.model_type in ["seg","segmentation"]: 
                train_writer,valid_writer,merged_summary = diag.prepare_summary_writer_seg( cgraph, FLAGS.summary_path );
            graph_saver = tf.train.Saver(max_to_keep=5, keep_checkpoint_every_n_hours=1.5);               
            global_step = tf.train.get_or_create_global_step();
    
    if is_chief and FLAGS.do_use_warminit and (oldvar is not None):
        graph = tf.get_default_graph();
#        variables_train = tf.trainable_variables()
        old_variables   = oldvar;
        assignment_list =[];
        for vv in old_variables:
            try:
                assignment_list.append( vv + [ tf.assign( graph.get_tensor_by_name(vv[0]), vv[1] )  ] );
            except Exception as e:
                print(e);
    else:
        assignment_list=[];
        
    return cgraph, graph_saver, global_step, loader, assignment_list, train_writer,valid_writer,merged_summary

    """Set up data preprocessing"""
def build_data_preproc():
    if FLAGS.model_type in ["cls","classification"]: 
        train_preproc = imagestream_cls.imagestream_cls(FLAGS.image_shuffle,FLAGS.image_prefetch,False,True,True,True).get_keypoints();
        valid_preproc = imagestream_cls.imagestream_cls(FLAGS.image_shuffle,FLAGS.image_prefetch,False,True,True,True).get_keypoints();
    elif FLAGS.model_type in ["seg","segmentation"]: 
        train_preproc = imagestream_seg.imagestream_seg(FLAGS.image_shuffle,FLAGS.image_prefetch,False,True,True,True).get_keypoints();
        valid_preproc = imagestream_seg.imagestream_seg(FLAGS.image_shuffle,FLAGS.image_prefetch,False,True,True,True).get_keypoints();
    return train_preproc,valid_preproc
    
    
    
    
    



def build_os_env():
    """Running the actual main function"""
    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"  
    os.environ["CUDA_VISIBLE_DEVICES"]="%d" % (FLAGS.gpu_index); 
        
    """"Setting up the output libraries"""
    FLAGS.savefile_path = FLAGS.savefile_path + '/' + FLAGS.run_name;
    try:
        os.mkdir( FLAGS.savefile_path );
    except:
        pass
    FLAGS.summary_path = FLAGS.summary_path + '/' + FLAGS.run_name;
    try:
        os.mkdir( FLAGS.summary_path );
    except:
        pass;

def init_loaded_parameters(sess, assignment_list,is_chief=True):
#    if is_chief and FLAGS.do_load_checkpoint:
#        datafile_name = '%s/%s' % ( FLAGS.loadfile_path, config.name_saved_model );    
#        loader.restore( sess, datafile_name );    
    if FLAGS.do_use_warminit:
        print("Starting warm init...")
        for entr in assignment_list:
            try:
                sess.run( entr[2] );
            except Exception as e:
                print(e)
        print("Done with warminit")



def init_data_handles(sess,iterator_tr,iterator_vl,files_tr,files_vl,filelist_train,filelist_valid,labels_tr,labels_vl ):
    handle_tr = sess.run( iterator_tr.string_handle() );
    handle_vl = sess.run( iterator_vl.string_handle() );
    if FLAGS.model_type in ["cls","classification"]: 
        sess.run( iterator_tr.initializer, feed_dict={ files_tr: filelist_train['filename'], labels_tr: filelist_train['label'] } );
        sess.run( iterator_vl.initializer, feed_dict={ files_vl: filelist_valid['filename'], labels_vl: filelist_valid['label'] } );
    elif FLAGS.model_type in ["seg","segmentation"]: 
        sess.run( iterator_tr.initializer, feed_dict={ files_tr: filelist_train['filename'], labels_tr: filelist_train['maskname'] } );
        sess.run( iterator_vl.initializer, feed_dict={ files_vl: filelist_valid['filename'], labels_vl: filelist_valid['maskname'] } );
    return handle_tr,handle_vl






def periodic_maintenance(sess, itr, elapsed, cgraph, graph_saver, global_step, merged_summary,train_writer,valid_writer,handle_tr, handle_vl):
    
    if FLAGS.model_type in ['cls','classification']:
        if (itr%FLAGS.write_summary)==1 and itr>FLAGS.write_summary:
            try:
                _, summary_val, lval_t, aval_t = sess.run( [cgraph.train_op,merged_summary,cgraph.res_loss,cgraph.res_accur], feed_dict={ cgraph.input_handle: handle_tr } );
                train_writer.add_summary(  summary_val, itr)
                summary_val, lval_v, aval_v = sess.run( [merged_summary,cgraph.res_loss,cgraph.res_accur], feed_dict={ cgraph.input_handle: handle_vl } );
                valid_writer.add_summary(summary_val, itr)
                print( "Iteration %d\tLoss:\t%g/%g\tAccuracy:\t%g/%g\t%g sec" %(itr, lval_t,lval_v,aval_t,aval_v,elapsed ) );
            except Exception as e:
                print(e)      
    elif FLAGS.model_type in ['seg','segmentation']:
        if (itr%FLAGS.write_summary)==1 and itr>FLAGS.write_summary:
            try:
                _, summary_val, lval_t, _, aval_t = sess.run( [cgraph.train_op,merged_summary,cgraph.res_loss,cgraph.res_iouc_tr,cgraph.res_ioum_tr], feed_dict={ cgraph.input_handle: handle_tr } );
                train_writer.add_summary(  summary_val, itr)
                summary_val, lval_v, _, aval_v = sess.run( [merged_summary,cgraph.res_loss,cgraph.res_iouc_te,cgraph.res_ioum_te], feed_dict={ cgraph.input_handle: handle_vl } );
                valid_writer.add_summary(summary_val, itr)
                print( "Iteration %d\tLoss:\t%g/%g\tIoU:\t%g/%g\t%g sec" %(itr, lval_t,lval_v,aval_t,aval_v,elapsed ) );
            except Exception as e:
                print(e)
                       
    if (itr%FLAGS.save_frequency)==1 and itr>FLAGS.save_frequency:
        savename = FLAGS.savefile_path + '/' + FLAGS.run_name;
        graph_saver.save(sess._tf_sess(),savename, global_step=global_step);                
                        
            




