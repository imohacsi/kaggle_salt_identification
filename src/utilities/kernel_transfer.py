# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 10:31:20 2018

@author: mohacsii
"""
import tensorflow as tf 
import numpy as np

def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text  # or whatever



#"""Creates a dictionary of old variable names and values before defining the new graph."""""
def warminit_oldvar_dict( path_saved_model, name_saved_model, verbose=False ):
#    path_saved_model = '../../savedmodels';
#    name_saved_model = 'HydraRun_240px-30002';

    tf.reset_default_graph();
    sess = tf.InteractiveSession();    

    """"Loading the old model with already trained weights"""
    metafile_name = '%s/%s.meta' % ( path_saved_model, name_saved_model );    
    oldloader = tf.train.import_meta_graph( metafile_name, import_scope='oldmodel', clear_devices=True );   
    datafile_name = '%s/%s' % ( path_saved_model, name_saved_model );    
    oldloader.restore( sess,  datafile_name );
    
    variables_train= tf.trainable_variables()
    
    vardict = [];
    for var in variables_train:
        varname = remove_prefix( var.name ,'oldmodel/');
        try:
            varval = np.array( sess.run( var ) );
            vardict.append( [varname, varval] );
            if verbose:
                print( "Succesfully read variable: ", varname );
        except:
            if verbose:
                print( "Whoops, somethings is wrong with: ", varname );
        
    """Need to clear graph due to stupid TF limitation"""
    sess.close();
    tf.reset_default_graph();

    return vardict;
    
def warminit_newvar_assign( sess, oldvar_list, verbose=1 ):   
    variables_train= tf.trainable_variables()
    
    for var_new in variables_train:
        for ovar in oldvar_list:
            if var_new.name == ovar[0]:
                try:
                    ass_val = tf.assign( var_new, ovar[1] );
                    sess.run( ass_val );
                    if verbose>1:
                        print( "Succesfully copied: ", var_new.name );
                except Exception as e:
                    if verbose>0:
                        print( "Whoops, somethings is wrong with: ", var_new.name );
                        print(e)
                
                
            
            
            
            
            
            
            
            
            
            
            
            




