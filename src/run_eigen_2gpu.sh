PS_HOSTS=eigen01:4999
WORKER_HOSTS=eigen01:5000,eigen01:5001


sleep 1
python distribute_model_ps.py -ps_hosts=$PS_HOSTS -worker_hosts=$WORKER_HOSTS & 
sleep 5
python distribute_model_worker.py -task_index=0 -gpu_index=0 -ps_hosts=$PS_HOSTS -worker_hosts=$WORKER_HOSTS &
sleep 5
python distribute_model_worker.py -task_index=1 -gpu_index=1 -ps_hosts=$PS_HOSTS -worker_hosts=$WORKER_HOSTS &

