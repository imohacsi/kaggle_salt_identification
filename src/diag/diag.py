# -*- coding: utf-8 -*-

import tensorflow as tf
import numpy as np
#import matplotlib.pyplot as plt


def print_num_trainpar( opath=None ):
    varlist = tf.trainable_variables();
    num_vars=0;
    
    for var in varlist:
        num_vars+= np.prod( np.array( var.get_shape().as_list() ) );
    print("Total number of trained variables in this session is: %0.3f Mpar" % (float(num_vars)/1000000) );
            
    sizelist = [];
    for var in varlist:
        sizelist.append( [var.name, np.prod( np.array( var.get_shape().as_list() ) )] )
    
    logfile = open( opath+'/trainable_parameters.txt', 'w');
    for itm in sizelist:
        logfile.write( "%s\t%s\n" % (itm[1], itm[0] ) );
    logfile.close();

    return num_vars

def prepare_summary_writer_cls( cgraph, summaries_path ):
    with tf.name_scope("summary"):
        
        try:
            tf.summary.scalar( 'accuracy', cgraph.res_accur );
        except:
            pass;
        tf.summary.scalar( 'loss_xe',  cgraph.res_loss_xe );
        tf.summary.scalar( 'loss_sl',  cgraph.res_loss_sl );
        tf.summary.scalar( 'loss_l2',  cgraph.res_loss_l2 );
        tf.summary.scalar( 'loss',     cgraph.res_loss );
        tf.summary.scalar( 'learning_rate', cgraph.res_lrate );
        try:
            in_list = tf.unstack(cgraph.input_images, axis=3)
            in_imag = tf.expand_dims( in_list[0], axis=-1 )
            tf.summary.image( 'input', in_imag, max_outputs=2 );
        except:
            pass;
    merged_summary = tf.summary.merge_all()
    train_writer = tf.summary.FileWriter( summaries_path + '/train')
    valid_writer = tf.summary.FileWriter( summaries_path + '/test')
    return train_writer,valid_writer,merged_summary

def prepare_summary_writer_seg( cgraph, summaries_path ):
    with tf.name_scope("summary"):
        try:
            tf.summary.scalar( 'iou_mean_tr', cgraph.res_ioum_tr );
            tf.summary.scalar( 'iou_mean_te', cgraph.res_ioum_te );
        except:
            pass;
        tf.summary.scalar( 'loss_xe',  cgraph.res_loss_xe );
        tf.summary.scalar( 'loss_sl',  cgraph.res_loss_sl );
        tf.summary.scalar( 'loss_l2',  cgraph.res_loss_l2 );
        tf.summary.scalar( 'loss',     cgraph.res_loss );
        tf.summary.scalar( 'learning_rate', cgraph.res_lrate );
        try:
            in_list = tf.unstack(cgraph.input_images, axis=3)
            in_imag = tf.expand_dims( in_list[0], axis=-1 )
            tf.summary.image( 'input', in_imag, max_outputs=2 );
        except:
            pass;
        try:
            tf.summary.image( 'mask', tf.expand_dims(cgraph.input_labels, -1), max_outputs=2 );
        except:
            pass;
#        try:
#        if tf.rank(cgraph.res_pred) == 4:
        predicted_map = tf.expand_dims( tf.argmax( cgraph.res_pred, axis=-1), axis=-1);
        tf.summary.image( 'pred', tf.cast(predicted_map, tf.float32), max_outputs=2 );
#        except:
#            pass;
    merged_summary = tf.summary.merge_all()
    train_writer = tf.summary.FileWriter( summaries_path + '/train')
    valid_writer = tf.summary.FileWriter( summaries_path + '/test')
    return train_writer,valid_writer,merged_summary
            

#
#class training_diag():
#    tline_itr_idx = [];    
#    tline_loss_tr = [];
#    tline_loss_vl = [];
#    tline_acc_tr = [];
#    tline_acc_vl = [];     
#    
#    def __init__(self, cgraph, handle_tr, handle_vl,  num_classes, do_plot=True):
#        self.cgraph = cgraph;
#        self.handle_tr = handle_tr;
#        self.handle_vl = handle_vl;
#        self.num_classes = num_classes;
#        self.do_plot = True;
#        
#        self.sess = tf.get_default_session();
#        self.dbg_loss_tr = -np.log( 1.0/num_classes );
#        self.dbg_acc_tr  = 1.0/num_classes;
#        self.dbg_loss_vl = -np.log( 1.0/num_classes );
#        self.dbg_acc_vl  = 1.0/num_classes;  
#        
#        
#        
#
#    def run(self, itr, t_per_iter):
#        
#        try:
#            loss_val,acc_val = self.sess.run( [self.cgraph.res_loss,self.cgraph.res_accur], feed_dict={ self.cgraph.input_handle: self.handle_tr } );
#        except:
#            loss_val,acc_val = self.sess.run( [self.cgraph.res_loss,self.cgraph.res_accur], feed_dict={ self.cgraph.input_handle: self.handle_tr } );
#        self.dbg_loss_tr = 0.9*self.dbg_loss_tr + 0.1*loss_val;
#        self.dbg_acc_tr  = 0.9*self.dbg_acc_tr  + 0.1*acc_val ; 
#
#        try:
#            loss_val,acc_val = self.sess.run( [self.cgraph.res_loss,self.cgraph.res_accur], feed_dict={ self.cgraph.input_handle: self.handle_vl } );
#        except:
#            loss_val,acc_val = self.sess.run( [self.cgraph.res_loss,self.cgraph.res_accur], feed_dict={ self.cgraph.input_handle: self.handle_vl } );
#        self.dbg_loss_vl = 0.9*self.dbg_loss_vl + 0.1*loss_val;
#        self.dbg_acc_vl  = 0.9*self.dbg_acc_vl  + 0.1*acc_val ;        
#        
#        self.tline_loss_tr.append( self.dbg_loss_tr );
#        self.tline_loss_vl.append( self.dbg_loss_vl );
#        self.tline_acc_tr.append( self.dbg_acc_tr );
#        self.tline_acc_vl.append( self.dbg_acc_vl );     
#        self.tline_itr_idx.append( itr );
#        print("Iter %d\tLoss: %g / %g \tAcc: %g / %g\tTime: %g s\n" % (itr, self.dbg_loss_tr, self.dbg_loss_vl, self.dbg_acc_tr, self.dbg_acc_vl,t_per_iter) );
#
#        if self.do_plot:
#            plt.figure(1);
#            plt.plot( self.tline_itr_idx, self.tline_loss_tr, self.tline_itr_idx, self.tline_loss_vl );
#            plt.ylim(0.0,10.0)
#            plt.grid(linewidth=2)
#            plt.show(block=False)
#            plt.pause(0.05)
#            plt.figure(2);
#            plt.plot( self.tline_itr_idx, self.tline_acc_tr, self.tline_itr_idx, self.tline_acc_vl );   
#            plt.ylim(0.0,1.0)
#            plt.grid(linewidth=2)
#            plt.show(block=False)
#            plt.pause(0.05)
