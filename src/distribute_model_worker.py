import tensorflow as tf
from graph import build_graph
from utilities import kernel_transfer
from utilities import multinodes
from utilities import init_ops
import sys,time

"""Select path for training and validation data"""
#datapath = '/gpfs/petra3/scratch/mohacsii/tiny-imagenet-200/train';
#datapath = 'C:\\Users\\mohacsii\\ColdStorage\\Data\\tiny-imagenet-200\\train';
#datapath = '/run/media/imohacsi/DataSSD/Kaggle/tiny-imagenet-200/train';
#datapath = '/run/media/imohacsi/DataSSD/Kaggle/GoogleChallenge/train';
#datapath = '/run/media/imohacsi/DataSSD/Kaggle/ILSVRC/Data/CLS-LOC/train';
datapath = "../data/train"


num_class = 2;

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string("worker_hosts","localhost:5000","""List of worker nodes""" );
tf.app.flags.DEFINE_string("ps_hosts","localhost:4999","""List of parameter servers""" );
tf.app.flags.DEFINE_string("job_name","worker","""Role of the current node""" );
tf.app.flags.DEFINE_integer("task_index",0,"""Number of the current node""" );
tf.app.flags.DEFINE_integer("gpu_index",0,"""Number of the current GPU""" );

#Basic learning environment variables
tf.app.flags.DEFINE_string("run_name","u_net_x0.5","""Prefix for save data""" );
tf.app.flags.DEFINE_string("summary_path","../summaries","""Prefix for save data""" );
tf.app.flags.DEFINE_string("savefile_path","../savefiles","""Folder for saving model checkpoints""" );
tf.app.flags.DEFINE_string("loadfile_path","../savefiles/u_net_x1.0","""Folder for reading previous checkpoints""" );
tf.app.flags.DEFINE_string("loadfile_name","u_net_x1.0-70701","""Name of the loaded checkpoint""" );
tf.app.flags.DEFINE_integer("do_use_warminit",0,"""Toggle to load a subset of variables from file""" );
tf.app.flags.DEFINE_integer("do_load_checkpoint",0,"""Toggle to resume previous checkpoint""" );

#Model style and data parameters
tf.app.flags.DEFINE_string( "model_type","seg","""Model type: classification or segmentation""" );
tf.app.flags.DEFINE_string( "data_style","tiny-imagenet","""Model type: classification or segmentation""" );
tf.app.flags.DEFINE_float(  "test_split",0.95,"""Fraction of data used for validation""" );

#Basic model parameters
tf.app.flags.DEFINE_string( "model_name","u_net","""Name of the model from the library""" );
tf.app.flags.DEFINE_float(  "model_width",0.5,"""Relative width of the model""" );
tf.app.flags.DEFINE_integer("batch_size",64,"""Number of images in a batch""" );
tf.app.flags.DEFINE_integer("last_step",300000,"""Number of total steps during training""" );
tf.app.flags.DEFINE_float(  "regularizer_l2" ,0.5e-4,"""L2 regularizer weight""" );
tf.app.flags.DEFINE_float(  "regularizer_sl" ,0.01,"""Xentropy regularizer weight""" );

#Learning parameters
tf.app.flags.DEFINE_integer("num_threads",4,"""Number of threads for preprocessing""" );
tf.app.flags.DEFINE_integer("image_shuffle",1000000,"""Number of filenames to be shuffled""" );
tf.app.flags.DEFINE_integer("image_prefetch",8,"""Number of batches to be prefetched""" );

tf.app.flags.DEFINE_integer("image_crop_x",101,"""Number of pixels bv   to be cropped from the image in X""" );
tf.app.flags.DEFINE_integer("image_crop_y",101,"""Number of pixels to be cropped from the image in Y""" );
tf.app.flags.DEFINE_integer("image_zoom",101,"""Number of pixels on the shorter edge of the resized image""" );
tf.app.flags.DEFINE_float( "lrate_init" ,0.05,"""Initial learning rate""" );
tf.app.flags.DEFINE_string("lrate_decay_type","expcos","""Learning rate decay method""" );

tf.app.flags.DEFINE_integer( "lrate_decay_per" ,10000,"""Learning rate decay length""" );
tf.app.flags.DEFINE_integer("lrate_ramp_up",5000,"""Learning rate ramp up period""" );
tf.app.flags.DEFINE_string("optimizer_type","mom","""Optimizer type""" );
#Debug options
tf.app.flags.DEFINE_integer( "write_summary" ,100,"""Write summary every N iteration""" );
tf.app.flags.DEFINE_integer( "save_frequency" ,5000,"""Save model every N iteration""" );



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""Running the actual main function"""
init_ops.build_os_env();

""""Loading variables from old initialization"""
if FLAGS.do_use_warminit:
    oldvar = kernel_transfer.warminit_oldvar_dict( FLAGS.loadfile_path, FLAGS.loadfile_name, False );
else:
    oldvar=None;

"""Read training file names"""
filelist_train,filelist_valid,num_class = init_ops.build_file_lists( datapath,num_class );

"""Establish nodes"""
device, target = multinodes.get_device_and_target();
is_chief = (FLAGS.task_index == 0);

"""Assigns ops to the local worker by default."""
with tf.device(device):
    if FLAGS.do_load_checkpoint==1:
        loader = build_graph.model_template.load_old_graph( FLAGS.loadfile_path, FLAGS.loadfile_name )  
    else:
        """Set up data preprocessing"""
        train_preproc,valid_preproc = init_ops.build_data_preproc();
        files_tr,labels_tr,_,iterator_tr = train_preproc;
        files_vl,labels_vl,_,iterator_vl = valid_preproc;

        """Build up graph"""
        graph_nodes = init_ops.build_graph_ops( device, is_chief, num_class, d_in=5, oldvar=oldvar );
        cgraph, graph_saver, global_step, loader, assignment_list, train_writer,valid_writer,merged_summary = graph_nodes;

        """Retrieve key training ops"""
        if FLAGS.model_type in ["seg","segmentation"]: 
            train_op,loss,_,_,input_handle = cgraph.get_basic_nodes();
        elif FLAGS.model_type in ["cls","classification"]: 
            train_op,loss,_,input_handle = cgraph.get_basic_nodes();        
        else:
            sys.exit("ERROR: Unknown model type: " + FLAGS.model_type );

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""Set up training hooks"""
hooks=[tf.train.StopAtStepHook(last_step=FLAGS.last_step )];  # The StopAtStepHook handles stopping after running given steps.
"""Set up the GPU computation environment"""
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8, allow_growth=True, allocator_type="BFC");
config = tf.ConfigProto(gpu_options=gpu_options,allow_soft_placement=False);

#The MonitoredTrainingSession takes care of session initialization, 
#   restoring from a checkpoint, saving to a checkpoint, and closing.   
with tf.train.MonitoredTrainingSession(master=target, is_chief=is_chief, hooks=hooks,
                                       save_checkpoint_secs=600, checkpoint_dir=None,
                                       save_summaries_steps=500, config=config ) as sess:    
    """Loading parameters from previous models (warminit)""" 
    if is_chief:
        init_ops.init_loaded_parameters(sess,assignment_list);
    
    """Initializing datastream"""    
    handle_tr,handle_vl = init_ops.init_data_handles(sess,iterator_tr,iterator_vl,files_tr,files_vl,
                                                     filelist_train,filelist_valid,labels_tr,labels_vl );

    """Run assynchronous training"""
    print("Beginning the training...")
    itr=0;
    elapsed = 1.0;
    while itr<FLAGS.last_step:
        itr+=1;            
        tstart = time.time();
        try:
            sess.run([train_op], feed_dict={ input_handle: handle_tr } );
        except Exception as e:
            print("There was a crash in training op...\n")
            print(e)
            sess.run([train_op], feed_dict={ input_handle: handle_tr } );
        elapsed = 0.3*(time.time() - tstart)+0.7*elapsed;
        
        if is_chief:
            init_ops.periodic_maintenance(sess,itr,elapsed,cgraph,graph_saver,global_step,merged_summary,
                                          train_writer,valid_writer,handle_tr, handle_vl);
            
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

