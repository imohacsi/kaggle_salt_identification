import cv2
import numpy as np
import matplotlib.pyplot as plt
from fileio import fileio



datapath = "../data/train"
filelist_raw = fileio.get_train_filelist_salt( datapath );

averages = []
for idx,fname in filelist_raw.iterrows():
	img = cv2.imread(fname['filename']);
	averages.append( np.mean( img/255.0 ) );

plt.hist( averages, 20 )
plt.show(block=False)

print( "Average label: ", np.mean(averages) );
