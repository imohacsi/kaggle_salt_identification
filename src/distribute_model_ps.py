import tensorflow as tf
import os

FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string("worker_hosts","localhost:5000","""List of worker nodes""" );
tf.app.flags.DEFINE_string("ps_hosts","localhost:4999","""List of parameter servers""" );
tf.app.flags.DEFINE_string("job_name","ps","""Role of the current node""" );
tf.app.flags.DEFINE_integer("task_index",0,"""Number of the current node""" );


def start_parameter_server():
    # If FLAGS.job_name is not set, we're running single-machine TensorFlow.
    if FLAGS.job_name is None:
        print("Running single-machine training")
        return (None, "")
    
    print("Parameter servers: ",FLAGS.ps_hosts)
    print("Worker nodes: ",FLAGS.worker_hosts)

    # Otherwise we're running distributed TensorFlow.
    print("Running distributed training")
    if FLAGS.task_index is None or FLAGS.task_index == "":
        raise ValueError("Must specify an explicit `task_index`")
    if FLAGS.ps_hosts is None or FLAGS.ps_hosts == "":
        raise ValueError("Must specify an explicit `ps_hosts`")
    if FLAGS.worker_hosts is None or FLAGS.worker_hosts == "":
        raise ValueError("Must specify an explicit `worker_hosts`")

    worker_nodes = FLAGS.worker_hosts.split(',');
    server_nodes = FLAGS.ps_hosts.split(',');
    
    #Create a cluster of parameter servers and worker hosts
    cluster = tf.train.ClusterSpec( { "worker": worker_nodes, "ps": server_nodes } );
    server = tf.train.Server( cluster, job_name=FLAGS.job_name, task_index=FLAGS.task_index );
    if FLAGS.job_name == "ps":
        print( "Starting a parameter server ", FLAGS.task_index );
        server.join();

#def main(_):
#    """Loading training and validation file lists from directory"""
#    start_parameter_server();



#if __name__ == "__main__":
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"  
os.environ["CUDA_VISIBLE_DEVICES"]=""    
"""Loading training and validation file lists from directory"""
start_parameter_server();

#    tf.app.run(main=main, argv=[sys.argv[0]])
