# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 02:48:59 2018

@author: mistvan
"""

import tensorflow as tf
from . import layers

def one_dense( x_in, num_class, act_fn=tf.nn.elu ):
    with tf.variable_scope("logits",reuse=None):    
        logits = tf.layers.dense( inputs=x_in, units=num_class, activation=act_fn );
        logits = tf.identity( logits, name='logits' );
    return logits;

def conv_dense( x_in, num_class, act_fn=tf.nn.elu ):
    with tf.variable_scope("logits",reuse=None):    
        logits = layers.conv2d( x_in, num_class, [1,1], act_fn=act_fn );
        logits = tf.identity( logits, name='logit_map' );
    return logits;

def softmax( logits ):
    with tf.variable_scope("predictions",reuse=None):    
        predictions = tf.nn.softmax( logits=logits, name='predictions' );
    return predictions;

def loss_xentropy( logits, labels):
    #Logits: Raw evidence coming trom the neural network
    #The actual training labels
    with tf.variable_scope("loss_xentr",reuse=None):    
        loss = tf.reduce_mean( tf.nn.softmax_cross_entropy_with_logits_v2( labels=labels, logits=logits ), name='loss_xentropy' );
    return loss;

def loss_weighted_xentropy( logits, labels, weights=None):
    #Logits: Raw evidence coming trom the neural network
    #The actual training labels
    with tf.variable_scope("loss_xentr",reuse=None):  

#        print( "labels shape: ", labels.get_shape().as_list() )
        unweighted_loss = tf.nn.softmax_cross_entropy_with_logits_v2( labels=labels, logits=logits );
        
#        print( "Unweighted shape: ", unweighted_loss.get_shape().as_list() )

        if weights is not None:
            class_weights = tf.constant(weights,name='class_weights', dtype=tf.float32);
            example_weights = tf.reduce_sum( class_weights*labels, axis=1 )
            print( "Weights shape: ", example_weights.get_shape().as_list() )
            loss = tf.reduce_mean( unweighted_loss*example_weights, name='loss_xentropy' );
        else:
            loss = tf.reduce_mean( unweighted_loss, name='loss_xentropy' );
        
    return loss;

def loss_self_prediction( logits, labels, weights=None, tempr=None, bsrate=0.0):
    if tempr is None:
        tempr=1.0;
    
    #Logits: Raw evidence coming trom the neural network
    #The actual training labels
    with tf.variable_scope("loss_self_entr",reuse=None):  
        
        bootstr_tempr = tf.constant(tempr,name='bootstrapping_temperature', dtype=tf.float32);
        bootstr_ratio = tf.constant(bsrate,name='bootstrapping_ratio', dtype=tf.float32);
        #Generating smoothened labels        
        softlabels = tf.nn.softmax( logits=(bootstr_tempr*logits) );      
#        print( "Softlabels: ", softlabels.get_shape().as_list() )
        
        unweighted_loss = tf.nn.softmax_cross_entropy_with_logits_v2( labels=softlabels, logits=logits );
        if weights is None:
            ent_loss = tf.reduce_mean( unweighted_loss );
        else:
            class_weights = tf.constant(weights,name='class_weights', dtype=tf.float32);        
            example_weights = tf.reduce_sum( class_weights*softlabels, axis=1 );            
            ent_loss = tf.reduce_mean( unweighted_loss*example_weights );
        loss = tf.identity( ent_loss, name='loss_self_entropy');         

    return bootstr_ratio*loss;


def loss_kernel_l2( w_decay ):
    trainable_var = tf.trainable_variables()
    regval_l2= tf.constant( w_decay, name='l2_regularizer' )
    kernel_var = [tvar for tvar in trainable_var if 'kernel' in tvar.name];
    kernel_l2  = [tf.nn.l2_loss(kvar) for kvar in kernel_var];
    added_loss = tf.add_n(kernel_l2)  
    l2_loss = tf.identity( tf.scalar_mul( regval_l2, added_loss), name='loss_l2');         
    return l2_loss;

def loss_mse( prediction, labels):
    with tf.variable_scope("loss_mse",reuse=None):  
        loss = tf.losses.mean_squared_error( labels, prediction );
        """Establishing the common diagnostics"""
        total_loss = tf.reduce_mean(loss, name='loss_mse')
    return total_loss;


def accuracy(predictions,labels):
    """Here we define the correctly classified fraction of the database """
    ndim = len(predictions.get_shape().as_list() )  
    with tf.variable_scope("accuracy",reuse=None):    
        correct_prediction = tf.equal( tf.argmax(predictions, ndim-1), tf.argmax( labels , ndim-1) )
        accuracy= tf.reduce_mean( tf.cast( correct_prediction, tf.float32 ) )  
        accuracy = tf.identity( accuracy, name='accuracy' );
    return accuracy;

def iou(predictions,labels):
    x_size = predictions.get_shape().as_list();

    """Here we calculate the IOU metric for segmentation """
    with tf.variable_scope("iou",reuse=None):    
        cls_pred = tf.argmax(predictions, axis=-1);
        cls_real = tf.argmax(labels,axis=-1);
        iou_mean,iou_conf = tf.metrics.mean_iou( cls_real, cls_pred, x_size[-1] );
        iou_mean = tf.identity( iou_mean, name='iou_mean' );
        iou_conf = tf.identity( iou_conf, name='iou_conf' );

    return iou_mean,iou_conf;

