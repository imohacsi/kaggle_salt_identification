# -*- coding: utf-8 -*-
"""
Created on Mon May 28 13:12:54 2018

@author: mohacsii
"""

import tensorflow as tf


"""Basic procedure of adding weights that are registered in the weights list"""
def add_weight( shape, initializer=tf.glorot_uniform_initializer() ):       
    """Allocate the variable"""
    if shape is not None:                       
        weight = tf.Variable( initializer(shape), dtype=tf.float32, name='kernel' )    
    else:
        assert False,"ERROR: Not enough parameters for bias initialization"
    
    """Add to collection of weights and return variable"""
    tf.add_to_collection("weight", weight)
    return weight;    
    
"""Basic procedure of adding bias that are registered in the bias list"""
def add_bias( shape=None ):
    """Change from initialization methods"""
    if shape is not None:                       #Initialize it to zero
        bias = tf.Variable( tf.zeros(shape), dtype=tf.float32, name='bias' );
    else:
        assert False,"ERROR: Not enough parameters for bias initialization"
    
    """Add to collection of biases and return variable"""
    tf.add_to_collection("bias", bias);         
    return bias;  

"""Basic procedure of adding an external feature vector as bias"""
def add_vector_bias( x_in, bias_vec ):
    vec_size = bias_vec.get_shape().as_list()[-1];
    x_size = x_in.get_shape().as_list()[-1];


#    if tf.rank( x_in )==4:
    mat_proj = add_weight( [vec_size,x_size] );
    dyn_bias = tf.matmul( bias_vec, mat_proj);            
    x_out = x_in + tf.expand_dims( tf.expand_dims(dyn_bias,1),1 );        
#    elif tf.rank( x_in )==2:
#        mat_proj = add_weight( [vec_size,x_size] );
#        dyn_bias = tf.matmul( bias_vec, mat_proj);            
#        x_out = x_in + dyn_bias;        
    return x_out;
    
    

def conv2d( x_in, filters, kernel_size, dilation=[1,1], strides=[1,1], padding='SAME', act_fn=tf.nn.elu, bias_vec=None ):
    """Specifying number of input channels"""
    x_filt = x_in.get_shape().as_list()[-1];
    w_size = [ kernel_size[0], kernel_size[1], x_filt,filters];
        
    with tf.variable_scope("conv",reuse=None):
        W = add_weight( w_size );            
        B = add_bias( [filters] );                     
        stride_rate = [1, strides[0], strides[1],1];
        dilate_rate = [1,1,dilation[0],dilation[1]];
        
        """Performing the actual convolution and adding postprocessing steps"""
        try:
            conv = tf.nn.conv2d(x_in, W, strides=stride_rate, padding=padding.upper(), dilations=dilate_rate );
        except:
            print("Ooops, something wrong with conv2d, trying legacy version...");
            conv = tf.nn.conv2d(x_in, W, strides=stride_rate, padding=padding.upper() );
        
        """Applying both biases"""
        conv = tf.nn.bias_add( conv, B);        
        if bias_vec is not None:          
            conv = add_vector_bias( conv, bias_vec );
                    
        """Just activate at the end"""
        if act_fn is not None:
            conv = act_fn( conv );
    return conv;

"""Groupped/Capsule convolution (based on ResNext)"""
def capconv2d( x_in, filters, kernel_size, dilation=[1,1], strides=[1,1], padding='SAME', act_fn=tf.nn.elu, bias_vec=None, csize=4, attention=True ):
    """Specifying number of capsules"""
    n_caps = int( filters/csize );
    
    intermed = [];
    for caps in range( n_caps ):
        caps = conv2d( x_in, csize, [1,1], act_fn=act_fn, bias_vec=bias_vec );
        caps = conv2d( caps, csize, [3,3], dilation=dilation, strides=strides, padding=padding, act_fn=act_fn );
        if attention:
            gate = tf.nn.sigmoid( conv2d( caps, 1, [1,1], act_fn=None ) );
            caps = caps*gate;
            
        intermed.append( caps );

    conv = tf.concat( intermed, axis=-1 );
    return conv;


def sepconv2d( x_in, filters, kernel_size, dilation=[1,1], strides=[1,1], padding='SAME', act_fn=tf.nn.elu, bias_vec=None ):
    """1x1 separable convolutions are useless"""
    if kernel_size[0]==1 and kernel_size[1]==1:
        return conv2d( x_in, filters, kernel_size, padding=padding, act_fn=act_fn );

    """Specifying input and output channel numbers"""
    x_filt = x_in.get_shape().as_list()[-1];
    
    w_d_size = [ kernel_size[0], kernel_size[1], x_filt, 1 ];
    w_p_size = [ 1, 1, x_filt, filters ];
    b_p_size = [ filters ]; 
    
    with tf.variable_scope("sepconv",reuse=None):
        W_d = add_weight( w_d_size );            
        W_p = add_weight( w_p_size );            
        B_p = add_bias(   b_p_size );  
        
        padding = padding.upper();
        stride = [1, strides[0], strides[1],1];
        #Performing the actual convolution and adding postprocessing steps
        sconv = tf.nn.separable_conv2d( x_in, W_d, W_p, strides=stride, padding=padding, rate=dilation );

        """If we are using external bias"""
        """Applying both biases"""
        sconv = tf.nn.bias_add( sconv, B_p);        
        if bias_vec is not None:          
            sconv = add_vector_bias( sconv, bias_vec );
            
            """Just activate at the end"""
        if act_fn is not None:
            sconv = act_fn( sconv );
    return sconv;
    

def dense( x_in, filters, act_fn=tf.nn.elu, dropout=None ):
    x_size = x_in.get_shape().as_list();
    if filters is None:
        filters = x_size[-1]
    else:
        filters = int( filters );
    
    
    with tf.variable_scope("dense",reuse=None):
        kernel = add_weight( [x_size[-1],filters] );            
        bias   = add_bias( [filters] );            
        ldense = act_fn( tf.nn.bias_add( tf.matmul( x_in, kernel),bias) );
        
        if dropout is not None:
            dropout_rate= tf.Variable( dropout, dtype=tf.float32, trainable=False, name="dropout_rate");
            ldense = tf.nn.dropout( ldense, dropout_rate );
    return ldense;



    
    











