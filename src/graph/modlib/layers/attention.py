# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 11:53:03 2018

@author: mohacsii
"""
import tensorflow as tf
from . import layers
from . import pooling

def attention_average_global( x_in ):
    x_size = x_in.get_shape().as_list();
    gpooled = tf.reduce_mean(x_in,axis=[1,2]);
    
    x_out = layers.conv2d( x_in, x_size[-1], [1,1], act_fn=tf.nn.elu, bias_vec=gpooled );
    return x_out
    
    


def attention_recurrent_global( x_in ):
    x_size = x_in.get_shape().as_list();

    v_pool = pooling.pool_recurrent( x_in );
    v_pool = tf.layers.dense(v_pool, x_size[-1] )
    
    x_map = layers.conv2d( x_in, x_size[-1],[1,1], bias_vec=v_pool )
    return x_map;