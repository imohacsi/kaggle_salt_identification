import numpy as np
import tensorflow as tf
from . import model_template_seg
from ..layers import pooling
from ..layers import layers

class deeplab_v3( model_template_seg.model_template_seg ):
       
    def _residual_block_v2( self, x_in, filters, dilation=1 ):
        x_size= x_in.get_shape().as_list();
        b1 = self._conv( x_in, filters=filters, kernel_size=[1,1], padding="SAME" );
        b2 = self._conv( b1, filters=filters, kernel_size=[3,3], padding="SAME", dilation=dilation );
        b3 = self._conv( b2, filters=x_size[-1], kernel_size=[1,1], padding="SAME", act_fn=None );
        
        return x_in+b3;

    def _asp_pool_block( self, x, filters=None, rate=[1,2,3] ):
        x_size= x.get_shape().as_list()[-1];
        if filters is None:
            filters = int(x_size);

        #Create the spatial pooling pyramid
        sp_list = [ self._conv( x, x_size, [1,1], padding="SAME") ];
        for scale in rate:
            sp_list.append( self._conv( x, x_size, [3,3], padding="SAME", dilation=scale ) );
        #Merge the spatial pooling pyramid
        xm = tf.concat( sp_list, axis=-1 );
        #Assign output depth
        x_out =self._conv( xm, filters, [1,1], padding="SAME");
        return x_out; 

    def _build_model( self, x_in ):       
#        x_in = tf.image.resize_images( x_in, [101,101]);
        
        with tf.variable_scope('conv1'):
            c1 = self._conv( x_in, filters=int(self.model_width*64), kernel_size=[7,7], padding="SAME" );
            c1 = self._conv( c1,   filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
        
        with tf.variable_scope('conv2'):
            c2 = tf.nn.pool( c1, [3,3], strides=[2,2], pooling_type='MAX', padding="SAME");
            c2 = self._residual_block_v2( c2, filters=int(self.model_width*64) );
            c2 = self._residual_block_v2( c2, filters=int(self.model_width*64) );
            c2 = self._residual_block_v2( c2, filters=int(self.model_width*64) );

        with tf.variable_scope('conv3'):
#            c3 = pooling.pool_dual( c2, pool_out=int(self.model_width*256) );
            c3 = self._residual_block_v2( c2, filters=int(self.model_width*128), dilation=2 );
            c3 = self._residual_block_v2( c3, filters=int(self.model_width*128), dilation=2 );
            c3 = self._residual_block_v2( c3, filters=int(self.model_width*128), dilation=2 );
            c3 = self._residual_block_v2( c3, filters=int(self.model_width*128), dilation=2 );

        with tf.variable_scope('conv4'):
            c4 = self._residual_block_v2( c3, filters=int(self.model_width*256), dilation=4 );
            c4 = self._residual_block_v2( c4, filters=int(self.model_width*256), dilation=4 );
            c4 = self._residual_block_v2( c4, filters=int(self.model_width*256), dilation=4 );
            c4 = layers.attention_global( c4 );
            c4 = self._residual_block_v2( c4, filters=int(self.model_width*256), dilation=4 );
            c4 = self._residual_block_v2( c4, filters=int(self.model_width*256), dilation=4 );
            c4 = self._residual_block_v2( c4, filters=int(self.model_width*256), dilation=4 );

#        with tf.variable_scope('conv5'):
#            c5 = self.residual_block_v2( c4, filters=int(self.model_width*512), dilation=4 );
#            c5 = self.residual_block_v2( c5, filters=int(self.model_width*512), dilation=4 );
#            c5 = self.residual_block_v2( c5, filters=int(self.model_width*512), dilation=4 );

#        with tf.variable_scope('assp1'):
#            a1 = self._asp_pool_block( c4, int(self.model_width*512), rate=[3,6,9] );
#        print(a1)

#        with tf.variable_scope('conv6'):
#            c6 = self._residual_block_v2( a1, filters=int(self.model_width*256), dilation=4 );
#            c6 = self._residual_block_v2( c6, filters=int(self.model_width*256), dilation=4 );
#            c6 = self._residual_block_v2( c6, filters=int(self.model_width*256), dilation=4 );
#        print(c6)

#        with tf.variable_scope('deconv3'):
#            dc3 = pooling.depool( c4, pool_out=int(self.model_width*256), output_shape=tf.shape(c3) );
#            dc3 = tf.concat( [c3,dc3], axis=-1 );
#            dc3 = self._conv( dc3, filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );
#            dc3 = self._conv( dc3, filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );	
            
#        with tf.variable_scope('deconv2'):
#            dc2 = pooling.depool( c6, pool_out=int(self.model_width*128), output_shape=tf.shape(c2) );
#            dc2 = tf.concat( [c2,dc2], axis=-1 );
#            dc2 = self._conv( dc2, filters=int(self.model_width*128), kernel_size=[3,3], padding="SAME" );
#            dc2 = self._conv( dc2, filters=int(self.model_width*128), kernel_size=[3,3], padding="SAME" );
        
        with tf.variable_scope('deconv1'):
            dc1 = pooling.depool( c4, pool_out=int(self.model_width*64), output_shape=tf.shape(c1), padding="SAME" );
            dc1 = tf.concat( [c1,dc1], axis=-1 );
            dc1 = self._conv( dc1, filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            dc1 = self._conv( dc1, filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );

        """This was the last deconvolved layer"""
        x = dc1;
		            
        """Agregate sparial features for postprocessing"""
        with tf.variable_scope("features",reuse=True):
            x = tf.identity( x, name='feature_map');
        print('Feature map dimensions: ', x.get_shape().as_list() );
        return x;

            
            
            
