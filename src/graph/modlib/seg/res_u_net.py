import numpy as np
import tensorflow as tf
from . import model_template_seg
from ..layers import pooling
from ..layers import attention
from ..layers import layers

class res_u_net( model_template_seg.model_template_seg ):


    def _residual_block( self, x ):
        x_size= x.get_shape().as_list()[-1];
        shortcut = x;
        x = tf.layers.batch_normalization(x)
        x = tf.nn.elu( x );        
        x = self._conv( x, x_size, [3,3], padding="SAME", act_fn=None );
        x = tf.layers.batch_normalization(x)
        x = tf.nn.elu( x );
        x = self._conv( x, x_size, [3,3], padding="SAME", act_fn=None );
        return x+shortcut;          
      
    def _build_model( self, x_in ):       
        
        
        
#        x_in = tf.image.resize_images( x_in, [101,101]);    

        with tf.variable_scope('conv1'):
            c1 = self._conv( x_in, filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
            c1 = self._conv( c1,    filters=int(self.model_width*64), kernel_size=[3,3], padding="SAME" );
        
        with tf.variable_scope('conv2'):
            c2 = pooling.pool_dual( c1, pool_out=int(self.model_width*128) );
            c2 = self._residual_block( c2 );

        with tf.variable_scope('conv3'):
            c3 = pooling.pool_dual( c2, pool_out=int(self.model_width*256) );
            c3 = self._residual_block( c3 );
            
#        with tf.variable_scope('conv4'):
#            c4 = pooling.pool_dual( c3, pool_out=int(self.model_width*512) );
#            c4 = self._conv( c4, filters=int(self.model_width*512), kernel_size=[3,3], padding="SAME" );
#            c4 = self._conv( c4, filters=int(self.model_width*512), kernel_size=[3,3], padding="SAME" );	            		
            
        with tf.variable_scope('bridge'):
            br = pooling.pool_dual( c3, pool_out=int(self.model_width*512) );
            br = self._residual_block( br );
            br = attention.attention_recurrent_global( br );
            br = self._residual_block( br );	
            br_o = pooling.depool( br, pool_out=int(self.model_width*256), output_shape=tf.shape(c3)  );
            
#        with tf.variable_scope('deconv4'):
#            dc4 = tf.concat( [c4,br_o], axis=-1 );
#            dc4 = self._conv( dc4, filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );
#            dc4 = self._conv( dc4, filters=int(self.model_width*256), kernel_size=[3,3], padding="SAME" );	
#            dc4_o = pooling.depool( dc4, pool_out=int(self.model_width*128), output_shape=tf.shape(c3)  );

        with tf.variable_scope('deconv3'):
            dc3 = tf.concat( [c2,br_o], axis=-1 );
            dc3 = self._residual_block( dc3 );
            dc3_o = pooling.depool( dc3, pool_out=int(self.model_width*128), output_shape=tf.shape(c2)  );


        with tf.variable_scope('deconv2'):
            dc2 = tf.concat( [c2,dc3_o], axis=-1 );
            dc2 = self._residual_block( dc2 );
            dc2_o = pooling.depool( dc2, pool_out=int(self.model_width*64), output_shape=tf.shape(c1)  );
            
        with tf.variable_scope('deconv1'):
            dc1 = tf.concat( [c1,dc2_o], axis=-1 );
            dc1 = self._residual_block( dc1 );
            dc1 = self._residual_block( dc1 );	
#        print( dc1 )
                	
        """This was the last deconvolved layer"""
        x = dc1;       
		
        """Assigning custom name to final feature layer"""    
        x =  tf.identity( x, name="conv_last" )
            
        """Agregate sparial features for postprocessing"""
        with tf.variable_scope("features",reuse=True):
            x = tf.identity( x, name='feature_map');
        print('Feature map dimensions: ', x.get_shape().as_list() );
        return x;

            
            
            