
import numpy as np
import tensorflow as tf
from . import model_template
from ..layers import pooling

class resnet50( model_template.model_template ):
    
    def _residual_block( self, x, depth ):
        x_size= x.get_shape().as_list()[-1];
        shortcut = x;
        x = self._conv( x, depth, [1,1]);
        x = self._conv( x, depth, [3,3]);
        x = self._conv( x, x_size, [3,3], act_fn=None);

        return x+shortcut;        
        
    def _residual_pool_block( self, x, filters=None ):
        x_size= x.get_shape().as_list()[-1];
        if filters is not None:
            if filters == x_size:
                shortcut = pooling.pool( x, "maxpool");
            else:
                shortcut = self._conv( x, filters, [1,1],  padding="SAME",strides=2);
        
        x = self._conv( x, x_size, [3,3], padding="SAME",strides=2);
        x = self._conv( x, 2*x_size, [3,3]);
        return x+shortcut;             
    
    def _build_model( self, x_in ):         
        with tf.variable_scope('conv1'):
            x = self._conv( x_in, filters=int(self.model_width*64), kernel_size=[7,7], strides=2 );
            x = pooling.pool( x, "maxpool");
                
        with tf.variable_scope('conv2'):
            x = self._residual_block( x, depth=int(self.model_width*64/4) );
            x = self._residual_block( x, depth=int(self.model_width*64/4) );
            x = self._residual_block( x, depth=int(self.model_width*64/4) );

        with tf.variable_scope('conv3'):
            x = self._residual_pool_block( x, int(self.model_width*128) );
            x = self._residual_block( x, depth=int(self.model_width*128/4) );
            x = self._residual_block( x, depth=int(self.model_width*128/4) );
            x = self._residual_block( x, depth=int(self.model_width*128/4) );
            x = self._residual_block( x, depth=int(self.model_width*128/4) );

        with tf.variable_scope('conv4'):
            x = self._residual_pool_block( x, int(self.model_width*256) );
            x = self._residual_block( x, depth=int(self.model_width*256/4) );
            x = self._residual_block( x, depth=int(self.model_width*256/4) );
            x = self._residual_block( x, depth=int(self.model_width*256/4) );
            x = self._residual_block( x, depth=int(self.model_width*256/4) );
            x = self._residual_block( x, depth=int(self.model_width*256/4) );
            x = self._residual_block( x, depth=int(self.model_width*256/4) );
            
        with tf.variable_scope('conv5'):
            x = self._residual_pool_block( x, int(self.model_width*512) );
            x = self._residual_block( x, depth=int(self.model_width*512/4) );
            x = self._residual_block( x, depth=int(self.model_width*512/4) );
            x = self._residual_block( x, depth=int(self.model_width*512/4) );
                    
        """Agregate sparial features for postprocessing"""
        with tf.variable_scope("global_pool",reuse=True):
            x = tf.reduce_mean(x, [1, 2], name='globalpool');
        with tf.variable_scope("features",reuse=True):
            x = tf.identity( x, name='feature_vector');
        print('Feature vector dimensions: ', x.get_shape().as_list() );
        return x;

            
            
            