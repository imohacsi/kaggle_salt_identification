import numpy as np
import tensorflow as tf
from . import model_template
from ..layers import pooling

class resnet18_assp( model_template.model_template ):
    
    def _residual_block( self, x, dilation=1 ):
        x_size= x.get_shape().as_list()[-1];
        shortcut = x;
        x = self._conv( x, x_size, [3,3], dilation=dilation );
        x = self._conv( x, x_size, [3,3], dilation=dilation );
        return x+shortcut;        

    def _asp_pool_block( self, x, filters=None, rate=[1,2,3] ):
        x_size= x.get_shape().as_list()[-1];
        if filters is None:
            filters = int(x_size);

        #Create the spatial pooling pyramid
        sp_list = [ self._conv( x, x_size, [1,1], padding="SAME") ];
        for scale in rate:
            sp_list.append( self._conv( x, x_size, [3,3], padding="SAME", dilation=scale ) );
        #Merge the spatial pooling pyramid
        xm = tf.concat( sp_list, axis=-1 );
        #Assign output depth
        x_out =self._conv( xm, filters, [1,1], padding="SAME");
        return x_out;     
    
    def _build_model( self, x_in ):         
        with tf.variable_scope('conv1'):
            x = self._conv( x_in, filters=int(self.model_width*64), kernel_size=[7,7], strides=2 );
            x = pooling.pool( x, "maxpool");
                
        with tf.variable_scope('conv2'):
            x = self._residual_block( x );
            x = self._residual_block( x );
            
        with tf.variable_scope('conv3'):
            x = self._asp_pool_block( x, filters=int(self.model_width*128), rate=[3,6,9] )            
            x = self._residual_block( x, dilation=2 );
            x = self._residual_block( x, dilation=2 );
            

        with tf.variable_scope('conv4'):
            x = self._asp_pool_block( x, filters=int(self.model_width*256), rate=[3,6,9] )
            x = self._residual_block( x, dilation=4 );
            x = self._residual_block( x, dilation=4 );
            
            
        with tf.variable_scope('conv5'):
            x = self._asp_pool_block( x, filters=int(self.model_width*512), rate=[3,6,9] )
            x = self._residual_block( x, dilation=8 );
            x = self._residual_block( x, dilation=8 );
            
        """Assigning custom name to final feature layer"""    
        x =  tf.identity( x, name="conv_last" )
            
        """Agregate sparial features for postprocessing"""
        with tf.variable_scope("global_pool",reuse=True):
            x = tf.reduce_mean(x, [1, 2], name='globalpool');
        with tf.variable_scope("features",reuse=True):
            x = tf.identity( x, name='feature_vector');
        print('Feature vector dimensions: ', x.get_shape().as_list() );
        return x;

            
            
            