

import tensorflow as tf
from ..layers import final_ops
from ..layers import layers

class model_template:
    model_width = 1.0;
    class_weights=None;
    regularizer_l2 = 0.0;
    regularizer_sl = 0.0;
    
    
    def __init__( self, num_classes, reg_l2=0.0, reg_self=0.0 ):
        self.num_classes = num_classes;
        self.regularizer_l2 = reg_l2;
        self.regularizer_sl = reg_self;
        
    def create_new_model(self, d_in=3 ):
        with tf.variable_scope("model",reuse=False):
            x_in,labels =   self._build_input_string_handle( d_in );
            logits =        self._build_model( x_in );
            loss =          self._build_final_op( logits, labels );
            self.train_op = self._build_training_op( loss );
    
    def load_old_graph( self, path, name ):
        metafile_name = '%s/%s.meta' % ( path, name)        
        loader = tf.train.import_meta_graph( metafile_name );   
        return loader;
    
    def __str__( self ):
        return "This is a general TF model template class."

    def _conv( self, x_in, filters, kernel_size, style='conv', dilation=1, strides=1, padding='SAME', act_fn=tf.nn.elu, bias_vec=None ):
        """Make sure that input is correct"""
        filters = int( filters );

        """Also accept simple numerical values"""
        if not isinstance(dilation, list):
            dilation = [dilation,dilation];
        if not isinstance(strides, list):
            strides = [strides,strides];

        if style=='conv':
            return layers.conv2d(    x_in, filters, kernel_size, dilation, strides, padding, act_fn, bias_vec = bias_vec );
        if style=='capconv':
            return layers.capconv2d( x_in, filters, kernel_size, dilation, strides, padding, act_fn, bias_vec = bias_vec );
        if style=='sepconv':
            return layers.sepconv2d( x_in, filters, kernel_size, dilation, strides, padding, act_fn, bias_vec = bias_vec );
            
    def _full( self, x_in, filters, act_fn, dropout ):
        return layers.dense( x_in, filters, act_fn, dropout );

    def _build_input_string_handle( self, d_in=3 ):
        with tf.variable_scope("input",reuse=None):
            """Merging placeholders with string handle"""
            print("\nInput handle dimension is: ",d_in);
            
            handle = tf.placeholder(tf.string, shape=[], name='input_handle');
            iterator =  tf.data.Iterator.from_string_handle( handle, (tf.float32,tf.int32), (tf.TensorShape([None,None,None,d_in]), tf.TensorShape([None])) );
            x_raw, y_raw = iterator.get_next();
            x_in = tf.identity( x_raw, name='examples');
#            x_in = tf.reshape(x_in, [-1, config.image_size[0],config.image_size[1],3])
            y_in = tf.identity( y_raw, name='labels' );
            labels = tf.one_hot( y_in, depth=self.num_classes );    
            return x_in,labels

    def _build_model( self, x_in ):
        print( "Dummy model from template!")
        
    def _build_final_op( self, x_in, labels ):
        with tf.variable_scope("finals",reuse=False):
            print(x_in)
            logits   = tf.identity( final_ops.one_dense ( x_in, self.num_classes ), name='logits' );
            loss_xen = final_ops.loss_weighted_xentropy( logits, labels );
            loss_l2  = final_ops.loss_kernel_l2( self.regularizer_l2 );
            loss_sen = final_ops.loss_self_prediction( logits, labels, self.class_weights, tempr=0.5, bsrate=self.regularizer_sl )
            loss     = tf.identity( loss_xen +loss_l2+loss_sen, name='loss');    
            final_ops.accuracy( logits, labels );
            final_ops.softmax( logits );   
            return loss;
        
    def _build_training_op( self, loss ):
        FLAGS = tf.app.flags.FLAGS
        with tf.variable_scope("train",reuse=False):
            train_op,lrate = self.set_optimizer( loss=loss, lrate_init=FLAGS.lrate_init, opt_par=FLAGS.optimizer_type, decay_type=FLAGS.lrate_decay_type, decay_per=FLAGS.lrate_decay_per, num_iter=FLAGS.last_step );
        return train_op;
        print( "Dummy training op from template!")
        

    def set_optimizer( self, loss, lrate_init, opt_par='mom', decay_type='exp', decay_per=2000, num_iter=None ):
        global_step = tf.train.get_or_create_global_step();
        FLAGS = tf.app.flags.FLAGS
        ramp_up = FLAGS.lrate_ramp_up;
        
        with tf.variable_scope("optimizer",reuse=None):  
            if decay_type=='lincos':
                num_periods = int( num_iter/decay_per );
                learning_rate = tf.train.linear_cosine_decay( lrate_init, global_step, decay_steps=num_iter, num_periods=num_periods, alpha=1e-8, beta=1e-8 );
            elif decay_type=='lineargrowth':
                learning_rate = lrate_init*( tf.to_float(global_step) /num_iter );            
            elif decay_type=='expcos':     
                print( "Decay period ",decay_per )
                num_periods = int( 100*num_iter/decay_per );
                cos_rate = tf.train.linear_cosine_decay( 0.5, (global_step-ramp_up), decay_steps=100*num_iter, num_periods=num_periods, alpha=1e-8, beta=0.75 );
                exp_rate = tf.train.exponential_decay( lrate_init, (global_step-ramp_up), decay_per/2, 0.95 );
                dlearning_rate = exp_rate*cos_rate;
                learning_rate = tf.cond( global_step<ramp_up, lambda: lrate_init*( tf.to_float(global_step)/ramp_up ),
                                                          lambda: dlearning_rate );                
            else:
                learning_rate = tf.train.exponential_decay( lrate_init, global_step, decay_per, 0.85 );
            learning_rate = tf.identity( learning_rate, name='decayed_learning_rate');
    
            if opt_par=='mom':
                optimizer =  tf.train.MomentumOptimizer(learning_rate,0.90);
            elif opt_par=='adam':
                optimizer =  tf.train.AdamOptimizer(learning_rate);
            else:
                optimizer =  tf.train.GradientDescentOptimizer(learning_rate);
                    
            train = optimizer.minimize( loss, global_step=global_step, name='minimize' );
        return train,learning_rate;



    def load_graph_checkpoints( self ):
        graph = tf.get_default_graph();
        self.input_handle = graph.get_tensor_by_name("model/input/input_handle:0");  
        try:
            self.input_images = graph.get_tensor_by_name("model/input_image:0");      
        except:
            pass;

        self.vec_feat = graph.get_tensor_by_name("model/features/feature_vector:0");

        self.res_loss_sl = graph.get_tensor_by_name("model/finals/loss_self_entr/loss_self_entropy:0");
        self.res_loss_xe = graph.get_tensor_by_name("model/finals/loss_xentr/loss_xentropy:0");
        self.res_loss_l2 = graph.get_tensor_by_name("model/finals/loss_l2:0");
        self.res_loss  = graph.get_tensor_by_name("model/finals/loss:0");
        self.res_logit = graph.get_tensor_by_name("model/finals/logits/logits:0");
        self.res_accur = graph.get_tensor_by_name("model/finals/accuracy/accuracy:0");
        self.res_pred  = graph.get_tensor_by_name("model/finals/predictions/predictions:0");

        self.res_gstep = graph.get_tensor_by_name("model/train/global_step:0");
        self.res_lrate = graph.get_tensor_by_name("model/train/optimizer/decayed_learning_rate:0");
        self.train_op  = graph.get_tensor_by_name("model/train/optimizer/minimize:0");
        try:
            self.last_feature_map = graph.get_tensor_by_name("model/conv_last:0"); 
        except:
            pass;
        try:
            self.par_sentropy_reg  = graph.get_tensor_by_name("model/finals/loss_self_entr/bootstrapping_ratio:0");
            self.par_sentropy_temp = graph.get_tensor_by_name("model/finals/loss_self_entr/bootstrapping_temperature:0");
        except:
            pass;
        try:
            self.map_recolor = graph.get_tensor_by_name("model/sens/recolor:0");
        except:
            try:
                self.map_recolor = graph.get_tensor_by_name("model/sens_w/recolor:0");
            except:
                pass;
            
    def set_model_width( self, val ):
        self.model_width = val;

    def get_basic_nodes( self ):
        return self.train_op,self.res_loss,self.res_accur,self.input_handle;
		
		
