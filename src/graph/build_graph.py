# -*- coding: utf-8 -*-
"""
Created on Wed Mar  7 14:57:00 2018

@author: mohacsii
"""
import tensorflow as tf
import sys

from .modlib.cls import resnet18
from .modlib.cls import resnet18_assp
from .modlib.cls import resnet12
from .modlib.seg import u_net
from .modlib.seg import deeplab_v3


def get_model_from_library( model_name, num_classes, regularizer_l2=0.0, regularizer_sl=0.0, par=None, mtype='cls' ):
    if mtype in ['cls','classification']:
        if model_name == 'resnet18':
            return resnet18.resnet18( num_classes, regularizer_l2, regularizer_sl );
        elif model_name == 'resnet12':
            return resnet12.resnet12( num_classes, regularizer_l2, regularizer_sl );  
        elif model_name == 'resnet18_assp':
            return resnet18_assp.resnet18_assp( num_classes, regularizer_l2, regularizer_sl );   
        else:
            sys.exit( "ERROR: Unknown classification model with name: " + model_name );
    elif mtype in ['seg','segmentation']:
        if model_name == 'u_net':
            return u_net.u_net( num_classes, regularizer_l2, regularizer_sl ); 
        elif model_name == 'deeplab_v3':
            return deeplab_v3.deeplab_v3( num_classes, regularizer_l2, regularizer_sl ); 
        else:
            sys.exit( "ERROR: Unknown segmentation model with name: " + model_name );
    else:
        sys.exit( "ERROR: Unknown model type with name: " + mtype );
    
