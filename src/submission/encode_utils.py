# -*- coding: utf-8 -*-
"""
Utility functions for encoding semantic segmentation results for final submission

Created on Mon Aug  6 12:30:00 2018

@author: mohacsii
"""
import tensorflow as tf
import numpy as np
import pandas as pd
 
def encode_prediction( img, offs=1 ):
    bytes = img.reshape([-1], order='F');
    string = "";
    
    pos = 0;
    cnt = 0;
    cat = 0;
    
    for px in range(len(bytes)):        
        #Starting a new section
        if bytes[px]!=cat:
            #Append stream
            if cat!=0:
                string = string + ( "%d %d " % (pos+offs,cnt) );
            
            pos = px;
            cnt = 1;
            cat = bytes[px];
        else:
            cnt += 1;
    #Final flush
    if cat!=0:
        string = string + ( "%d %d " % (pos+offs,cnt) );
            
    return string;
            
            
def decode_prediction( string, shape, offs=1 ):
    size = np.prod( shape );
    bytes = np.zeros( size );
    #Early exit
    if len(string)==0:
        return np.reshape(bytes,shape);
    
    code=0;   
    string = string.split();
    while code < len(string)-1:
        idex = int(string[code])-offs;
        size = int(string[code+1]);
        bytes[idex:idex+size] = 1;
        code +=2;
    img = np.reshape(np.array(bytes),shape);
    return img;
                        
    
        