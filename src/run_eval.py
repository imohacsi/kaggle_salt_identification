# -*- coding: utf-8 -*-
"""
Created on Thu Feb  1 16:50:35 2018

@author: mohacsii
"""
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
import pandas as pd
import cv2
import time, os

from submission import encode_utils
from fileio import fileio_seg
from fileio import imagestream_seg
from graph.modlib.seg import model_template_seg as model


FLAGS = tf.app.flags.FLAGS

#Basic learning environment variables
tf.app.flags.DEFINE_string("encoding_path","../encodings","""Folder for reading previous checkpoints""" );
tf.app.flags.DEFINE_string("encoding_name","unet_x0.5_i146k","""Name of the loaded checkpoint""" );
tf.app.flags.DEFINE_string("loadfile_path","../savefiles/u_net_x0.5","""Folder for reading previous checkpoints""" );
tf.app.flags.DEFINE_string("loadfile_name","u_net_x0.5-146451","""Name of the loaded checkpoint""" );
#deeplab_v3-37876

tf.app.flags.DEFINE_integer("batch_size",1,"""Number of images in a batch""" );
tf.app.flags.DEFINE_integer("num_threads",4,"""Number of threads for preprocessing""" );
tf.app.flags.DEFINE_integer("image_crop_x",101,"""Number of pixels bv   to be cropped from the image in X""" );
tf.app.flags.DEFINE_integer("image_crop_y",101,"""Number of pixels to be cropped from the image in Y""" );
tf.app.flags.DEFINE_integer("image_zoom",101,"""Number of pixels on the shorter edge of the resized image""" );
tf.app.flags.DEFINE_integer("num_test_eval",64,"""Number of evaluations for final testing""" );

"""Running the actual main function"""
#if __name__ == "__main__":
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"  
os.environ["CUDA_VISIBLE_DEVICES"]="%d" % (1); 

"""Image data preprocessor pipeline"""
with tf.variable_scope("preprocessor",reuse=None):
    test_preproc = imagestream_seg.imagestream_seg(1,1,True,True,False,True).get_keypoints();
    files_te,test_set ,iterator_te = test_preproc;

"""Loading test images"""
filelist_test = fileio_seg.get_test_filelist( "../data/test/images" );
print("Number of testing examples: ", len(filelist_test) );

"""Loading pre-trained model"""
#tf.reset_default_graph();
cgraph = model.model_template_seg(24);

metafile_name = '%s/%s.meta' % ( FLAGS.loadfile_path, FLAGS.loadfile_name)        
loader = tf.train.import_meta_graph( metafile_name, clear_devices=True );  
    

"""Loading named entities from the graph"""
cgraph.load_graph_checkpoints();

tensor_names = [t.name for op in tf.get_default_graph().get_operations() for t in op.values()];



print("Initializing variables!")
init = tf.global_variables_initializer();
"""Set up the GPU computation environment"""
gpu_options = tf.GPUOptions(allow_growth=True, allocator_type="BFC");
gpuconfig = tf.ConfigProto(gpu_options=gpu_options,allow_soft_placement=True);
sess=tf.InteractiveSession( config=gpuconfig);
sess.run(init)
handle_te = sess.run( iterator_te.string_handle() )


#Restoring model parameters
datafile_name = '%s/%s' % ( FLAGS.loadfile_path, FLAGS.loadfile_name );
try:
    loader.restore( sess, datafile_name )
except Exception as e:
    print(e);
    pass;
    
THOLD = 0.5;
cvkernel = np.ones((3,3),np.uint8);
predictions = []; 
for idx,entry in filelist_test.iterrows():
    tstart = time.time();
    sess.run( iterator_te.initializer, feed_dict={ files_te: [entry['filename']]*FLAGS.num_test_eval } );
    
    prediction_map = sess.run( cgraph.res_pred, feed_dict={ cgraph.input_handle: handle_te } ); 
    prediction_prob = np.mean( prediction_map, axis=0 );
    prediction_map = np.array( prediction_prob );
    prediction_map[prediction_map>=THOLD] = 1;
    prediction_map[prediction_map<THOLD] = 0;
    prediction_map = np.argmax( prediction_map, axis=-1 );
    prediction_map = prediction_map.astype(np.uint8);
    prediction_map = cv2.morphologyEx(prediction_map, cv2.MORPH_CLOSE, cvkernel);
    
#    prediction_map = np.rot90( prediction_map, k=3 )
    
    encoded_map = encode_utils.encode_prediction( np.round(prediction_map) );  
    
    
#    decoded_map = np.array(encode_utils.decode_prediction(encoded_map,[101,101]));
#    if decoded_map.size!=101*101:
#        print( entry );
    
    predictions.append([entry['hash'], encoded_map]); 
    elapsed = time.time()-tstart;
    
    if idx%200==0:
        print("Calculated %d of %d in %f sec..." % (idx,len(filelist_test),elapsed) );
        plt.figure(1)
        plt.subplot(1,2,1)
        plt.imshow( prediction_map[:,:])
        plt.subplot(1,2,2)
        plt.imshow( prediction_prob[:,:,1], clim=(0.0,1.0) )    
        plt.suptitle( "Showing %d of %d" % (idx,len(filelist_test)) )
        plt.show(block=False)
        plt.pause(0.02)
        

df = pd.DataFrame( predictions, columns=['id','rle_mask'] );        
filename = FLAGS.encoding_path + '/' + FLAGS.encoding_name + '.csv';
print(filename);
df.to_csv( filename,index=False );


