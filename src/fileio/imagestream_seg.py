# -*- coding: utf-8 -*-
import tensorflow as tf
import numpy as np
from imgaug import augmenters as iaa 
FLAGS = tf.app.flags.FLAGS



seq_inplace = iaa.Sequential([
        iaa.AdditiveGaussianNoise(loc=0,scale=(0.0,6.0), per_channel=0.5),
        iaa.Invert(0.3, per_channel=True),
        iaa.SomeOf( (0,2),
            iaa.OneOf([
                iaa.GaussianBlur((0.0,1.5)),
                iaa.AverageBlur(k=(1,3)),
                iaa.MedianBlur(k=(1,3)) ]),
            iaa.OneOf([
                iaa.Sharpen(alpha=(0, 0.7), lightness=(0.75, 1.25)),
                iaa.Emboss(alpha=(0, 0.7), strength=(0, 1.0)) ]),                   
            iaa.Sometimes( 0.3, iaa.Dropout( (0.01,0.05), per_channel=0.5 ) ) )                  
        ], random_order=True);  

def wrap_inplace( x_in ):
    return seq_inplace.augment_image( x_in );

seq_morpho = iaa.Sequential([
#        iaa.CoarseDropout((0.0, 0.05), size_percent=(0.05, 0.25))
        iaa.Fliplr(0.5),
        iaa.Flipud(0.5),
#        iaa.Sometimes(iaa.Crop(percent=(0, 0.3))),
        iaa.Affine(
            scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
            translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)},
            rotate=(-22, 22),
            shear=(-8, 8),
            order=0,
            cval=(112, 144) )
            #mode=iaa.ALL 
#        iaa.Sometimes(0.5,iaa.PiecewiseAffine(scale=(0.01, 0.1)))
        ], random_order=True);    
           
def wrap_morpho( x_in, y_in ):
    seq_morpho_det = seq_morpho.to_deterministic();
    return seq_morpho_det.augment_image( x_in ),seq_morpho_det.augment_image( y_in );


class imagestream_seg():
    def __init__(self, data_shuffle, data_prefetch, is_test=False,
                 do_augment=False, do_loopit=True, do_whiten=False, do_prefetch=True, iter_type='initializable' ):
        self.image_size = tf.constant( [FLAGS.image_crop_x,FLAGS.image_crop_y], dtype=tf.int32 );
        self.image_zoom = tf.constant( FLAGS.image_zoom, dtype=tf.int32 );
        self.batch_size = FLAGS.batch_size;
        self.num_threads= FLAGS.num_threads;
        self.data_shuffle = data_shuffle;
        self.data_prefetch= data_prefetch;
        self.is_test = is_test;
        self.do_augment = do_augment;
        self.do_loopit = do_loopit;
        self.do_whiten = do_whiten;
        self.do_prefetch = do_prefetch;
        self.iter_type = iter_type;
        
        
        self.create_location_map( FLAGS.image_zoom, FLAGS.image_zoom );
        if self.is_test:
            self.set_up_pipeline_test();
        else:
            self.set_up_pipeline_train();
    
    
    def create_location_map( self, Nx, Ny ):
        x_vec = np.arange( Nx )*1.0/float(Nx);
        y_vec = np.arange( Ny )*1.0/float(Ny);
        
        xy_map = np.zeros( [Nx,Ny,2] );
        for xx in range(Ny):
            xy_map[xx,:,0] = x_vec;
        for yy in range(Nx):
            xy_map[:,yy,1] = y_vec;
                
        self.location_map = tf.Variable( xy_map, trainable=False, dtype=tf.float32 );        
                
    def set_up_pipeline_train( self ):
    
        self.plholder_fnames = tf.placeholder( tf.string, [None] );
        self.plholder_mnames = tf.placeholder( tf.string, [None] );
#        self.plholder_depth = tf.placeholder( tf.float32, [None] );

        self.data_set = tf.data.Dataset.from_tensor_slices( ( self.plholder_fnames, self.plholder_mnames ) );
        """Loop and repeat the filenames (not the images)"""
        if self.do_loopit:
            self.data_set = self.data_set.shuffle( buffer_size=self.data_shuffle );
            self.data_set = self.data_set.repeat( );       
        self.data_set = self.data_set.map( lambda fn,mn: _tf_read_image_all(fn,mn,self.image_size,self.image_zoom,self.do_augment,self.do_whiten,self.location_map), num_parallel_calls=self.num_threads );
        self.data_set = self.data_set.batch( self.batch_size );        
       
        if self.do_prefetch:
            self.data_set = self.data_set.prefetch( self.data_prefetch );
    
        if self.iter_type=='initializable':
            self.dataset_iterator = self.data_set.make_initializable_iterator();
        elif self.iter_type=='reinitializable':
            self.dataset_iterator = self.data_set.make_reinitializable_iterator();
        else:
            assert False,'ERROR: unknown iterator type for dataset.';        
    
    def set_up_pipeline_test( self ):
    
        self.plholder_fnames = tf.placeholder( tf.string, [None] );
#        self.plholder_depth = tf.placeholder( tf.float32, [None] );

        self.data_set = tf.data.Dataset.from_tensor_slices( ( self.plholder_fnames ) );
        """Loop and repeat the filenames (not the images)"""
        if self.do_loopit:
            self.data_set = self.data_set.shuffle( buffer_size=self.data_shuffle );
            self.data_set = self.data_set.repeat( );       
        self.data_set = self.data_set.map( lambda fn: _tf_read_image_test(fn,self.image_size,self.image_zoom,self.do_augment,self.do_whiten,self.location_map), num_parallel_calls=self.num_threads );
        self.data_set = self.data_set.batch( self.batch_size );        
       
        if self.do_prefetch:
            self.data_set = self.data_set.prefetch( self.data_prefetch );
    
        if self.iter_type=='initializable':
            self.dataset_iterator = self.data_set.make_initializable_iterator();
        elif self.iter_type=='reinitializable':
            self.dataset_iterator = self.data_set.make_reinitializable_iterator();
        else:
            assert False,'ERROR: unknown iterator type for dataset.';       
 
    def get_keypoints( self ):
        if self.is_test:
            return self.plholder_fnames,self.data_set,self.dataset_iterator;
        else:
            return self.plholder_fnames,self.plholder_mnames,self.data_set,self.dataset_iterator;


def _tf_read_image_all( filename, maskname, num_pixels, num_zoom, do_aug=True, do_whiten=True, loc_map=None ):
    image_bytecode = tf.read_file(filename);
    mask_bytecode  = tf.read_file(maskname);
    image_decoded = tf.image.decode_jpeg(image_bytecode, channels=3 );
    mask_decoded  = tf.image.decode_png(mask_bytecode, dtype=tf.uint16, channels=1 );   

    """Finally resize the image to the final scale"""
    img = tf.image.resize_images( image_decoded,  [num_zoom,num_zoom]);    
    mask= tf.image.resize_images( mask_decoded, [num_zoom,num_zoom]);       
    
    if do_aug:
        oishape = tf.shape( img );
        omshape = tf.shape( mask );
        #Doing the augmentation
        img         = tf.py_func( wrap_inplace, [img], tf.float32 );
        img, mask   = tf.py_func( wrap_morpho, [ img,mask ],  [tf.float32,tf.float32] );
        #Restoring shape
        img = tf.reshape( img, oishape );
        mask = tf.reshape( mask, omshape );

    """Randomly rotate original image by 90 deg"""
    if do_aug:
        coin = tf.less(tf.random_uniform((), 0., 1.), 0.5)
        img,mask = tf.cond( coin, lambda: (tf.image.rot90(img),tf.image.rot90(mask)), lambda: (img,mask) );

    if do_whiten:
        img = tf.image.per_image_standardization( img ); 
        mask = mask / 65535.0;
    else:
        img = tf.scalar_mul( (1.0/256.0), img );
        mask = mask / 65535.0;
        
    if do_aug:
        offset = tf.random_uniform((), 0., 1.)-0.5;
        scale = tf.random_uniform((), 0.5, 2.);
        img = tf.scalar_mul( scale, img );
        img = tf.add( img, offset );
        
    if loc_map is not None:
        img = tf.concat([img,loc_map], axis=-1 );
      
    return img,mask


def _tf_read_image_test( filename, num_pixels, num_zoom, do_aug=True, do_whiten=True, loc_map=None ):
    image_bytecode = tf.read_file(filename);
    image_decoded = tf.image.decode_jpeg(image_bytecode, channels=3 );
    mask_decoded  = tf.image.decode_jpeg(image_bytecode, channels=1 );
    
    """Finally resize the image to the final scale"""
    img = tf.image.resize_images( image_decoded, [num_zoom,num_zoom]);    
    mask= tf.image.resize_images( tf.zeros( tf.shape(mask_decoded) ),  [num_zoom,num_zoom]);    

    """Randomly crop a ROI from the image"""
    if do_aug:
        img = tf.py_func( wrap_inplace, [img], tf.float32 );

    if do_whiten:
        img = tf.image.per_image_standardization( img ); 
#        mask = tf.scalar_mul( (1.0/65535.0), mask );
        mask = mask / 65535;
    else:
        img = tf.scalar_mul( (1.0/256.0), img );
        mask = mask / 65535;
        
    if do_aug:
        offset = tf.random_uniform((), 0., 1.)-0.5;
        scale = tf.random_uniform((), 0.7, 1.5 );
        img = tf.scalar_mul( scale, img );
        img = tf.add( img, offset );
        
    if loc_map is not None:
        img = tf.concat([img,loc_map], axis=-1 );
      
    return img,mask

